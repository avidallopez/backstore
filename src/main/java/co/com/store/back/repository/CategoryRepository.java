package co.com.store.back.repository;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import co.com.store.back.entity.Category;

@Repository
public interface CategoryRepository extends PagingAndSortingRepository<Category, Integer> {
	@Query("select c from Category c where c.id=?1")
	Category findOneById(int id);
	
	@Query("select c from Category c where c.father is null")
	Page<Category> findFather(Pageable pageable);
	
	@Query("select c from Category c where c.father.id=?1")
	Page<Category> findCategoryByFather(int id, Pageable pageable);
	
	@Query("select c from Category c where (select count(c2) from Category c2 where c2.father.id = c.id) = 0")
	List<Category> findFinalSond();
	
	@Query("select c.father.id from Category c where c.id=?1")
	int getBack(int id);
	
	@Query("select count(c) from Category c where c.father.id =?1")
	int countSons(int id);
}
