package co.com.store.back.repository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import co.com.store.back.entity.User;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, String> {
	@Query("select u from User u where u.id=?1")
	User findOneById(String id);
	
	User findByUserName(String userName);
	
	
	

}
