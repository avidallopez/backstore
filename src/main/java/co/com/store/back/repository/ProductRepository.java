package co.com.store.back.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import co.com.store.back.entity.Product;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Integer> {
	@Query("select p from Product p where p.id=?1")
	Product findOneById(int id);
	
	@Query("select p from Product p where p.category.id=?1")
	Page<Product> findProductsByCategory(int id, Pageable pageable);

}
