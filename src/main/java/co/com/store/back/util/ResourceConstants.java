package co.com.store.back.util;

public class ResourceConstants {
	
	public final static String USER_RESOURCE= "/user";
	public final static String CATEGORY_RESOURCE= "/category";
	public final static String PRODUCT_RESOURCE= "/product";
	public final static String CURRENCY_RESOURCE= "/currency";
	public final static String SECRET_KEY="&%$·1234567ytrerwfgdhs";
	public final static int PAGE = 4;

}
