package co.com.store.back.auth;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

import co.com.store.back.util.ResourceConstants;

@SuppressWarnings("deprecation")
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		.antMatchers(HttpMethod.GET,ResourceConstants.USER_RESOURCE).hasRole("ADMIN")
		.antMatchers(HttpMethod.POST,ResourceConstants.USER_RESOURCE).hasRole("ADMIN")
		.antMatchers(HttpMethod.PUT,ResourceConstants.USER_RESOURCE).hasRole("ADMIN")
		.antMatchers(HttpMethod.DELETE,ResourceConstants.USER_RESOURCE).hasRole("ADMIN")
		.antMatchers(HttpMethod.POST,ResourceConstants.CATEGORY_RESOURCE).hasRole("ADMIN")
		.antMatchers(HttpMethod.PUT,ResourceConstants.CATEGORY_RESOURCE).hasRole("ADMIN")
		.antMatchers(HttpMethod.DELETE,ResourceConstants.CATEGORY_RESOURCE).hasRole("ADMIN")
		.antMatchers(HttpMethod.POST,ResourceConstants.PRODUCT_RESOURCE).hasRole("ADMIN")
		.antMatchers(HttpMethod.PUT,ResourceConstants.PRODUCT_RESOURCE).hasRole("ADMIN")
		.antMatchers(HttpMethod.DELETE,ResourceConstants.PRODUCT_RESOURCE).hasRole("ADMIN")
		.anyRequest().permitAll();
	}
	
}
