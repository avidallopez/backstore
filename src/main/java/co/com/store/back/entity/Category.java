package co.com.store.back.entity;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "category")
public class Category implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column
	private String name;
	
	
	@ManyToOne
	@JoinColumn(name="father")
	private Category father;
	
	@Column(length = 500)
	private String photo;

	public int getId() {
		return id; 
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Category getFather() {
		return father;
	}

	public void setFather(Category father) {
		this.father = father;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	

}
