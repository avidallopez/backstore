package co.com.store.back.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import co.com.store.back.entity.Category;

public interface CategoryService {
	List<Category> findAll();
	Page<Category> findFather(Pageable pageable);
	Page<Category> findByFather(int id, Pageable pageable);
	Category findCategory(int id);
	Category add(Category category);
	Category edit(Category category);
	Category delete(int id);
	List<Category> findFinalSon();
	int getBack(int id);
	int countSons(int id);
}
