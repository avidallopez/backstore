package co.com.store.back.service;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import co.com.store.back.entity.Product;


public interface ProductSetvice {
	Page<Product> findAll(Pageable pageable);
	Page<Product> finByCategory(int id, Pageable pageable);
	Product findProduct(int id);
	Product add(Product product);
	Product edit(Product product);
	Product delete(int id);
}
