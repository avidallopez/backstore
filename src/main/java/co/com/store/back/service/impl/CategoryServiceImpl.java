package co.com.store.back.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import co.com.store.back.entity.Category;
import co.com.store.back.repository.CategoryRepository;
import co.com.store.back.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Override
	public List<Category> findAll() {
		return (List<Category>) categoryRepository.findAll();
	}

	@Override
	public Category findCategory(int id) {
		return categoryRepository.findOneById(id);
	}

	@Override
	public Category add(Category category) {
		return categoryRepository.save(category);
	}

	@Override
	public Category edit(Category category) {
		return categoryRepository.save(category);
	}

	@Override
	public Category delete(int id) {
		Category category = this.findCategory(id);
		if (category != null) {
			categoryRepository.delete(category);
		}
		return category;
	}

	@Override
	public Page<Category> findFather(Pageable pageable) {
		return categoryRepository.findFather(pageable);
	}

	@Override
	public Page<Category> findByFather(int id, Pageable pageable) {
		return categoryRepository.findCategoryByFather(id, pageable);
	}

	@Override
	public List<Category> findFinalSon() {
		return categoryRepository.findFinalSond();
	}

	@Override
	public int getBack(int id) {
		try {
			int back = categoryRepository.getBack(id);
			return back;
		} catch (Exception e) {
			return 0;
		}
		
	}

	@Override
	public int countSons(int id) {
		return categoryRepository.countSons(id);
	}

}
