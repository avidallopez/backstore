package co.com.store.back.service;

import java.util.List;

import co.com.store.back.entity.User;

public interface UserService {
	List<User> listAll();
	User findUser(String idUser);
	User add(User user);
	User edit(User user);
	User delete(String idUser);
	
}
