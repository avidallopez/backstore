package co.com.store.back.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


import co.com.store.back.entity.Product;
import co.com.store.back.repository.ProductRepository;
import co.com.store.back.service.ProductSetvice;

@Service
public class ProductServiceImpl implements ProductSetvice {
	
	@Autowired
	private ProductRepository productRepository;

	@Override
	public Page<Product> findAll(Pageable pageable) {
		return  productRepository.findAll(pageable);
	}

	@Override
	public Product findProduct(int id) {
		try {
			return productRepository.findOneById(id);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Product add(Product product) {
		return productRepository.save(product);
	}

	@Override
	public Product edit(Product product) {
		return productRepository.save(product);
	}

	@Override
	public Product delete(int id) {
		Product product = this.findProduct(id);
		if (product != null) {
			productRepository.delete(product);
		}
		return product;
	}

	@Override
	public Page<Product> finByCategory(int id, Pageable pageable) {
		return productRepository.findProductsByCategory(id, pageable);
	}

}
