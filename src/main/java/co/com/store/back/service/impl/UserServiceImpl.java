package co.com.store.back.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.store.back.entity.User;
import co.com.store.back.repository.UserRepository;
import co.com.store.back.service.UserService;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {

	@Autowired
	private UserRepository userRepository;
	
	private Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Override
	public List<User> listAll() {
		return (List<User>) userRepository.findAll();
	}

	@Override
	public User findUser(String id) {
		return userRepository.findOneById(id);
	}

	@Override
	public User add(User user) {
		return userRepository.save(user);
	}

	@Override
	public User edit(User user) {
		return userRepository.save(user);
	}

	@Override
	public User delete(String idUser) {
		User user = this.findUser(idUser);
		if (user != null) {
			userRepository.delete(user);
		}
		return user;
	}

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		User user = userRepository.findByUserName(userName);
		
		if (user == null) {
			String msg = "User: "+ userName+ " not fount.";
			logger.error(msg);
			throw new UsernameNotFoundException(msg);
		}
		
		List<GrantedAuthority> authorities = user.getRoles().stream()
				.map(role -> new SimpleGrantedAuthority(role.getDescription())).collect(Collectors.toList());

		return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(), true,
				true, true, true, authorities);
	}

}
