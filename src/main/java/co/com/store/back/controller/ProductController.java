package co.com.store.back.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.store.back.entity.Product;
import co.com.store.back.service.ProductSetvice;
import co.com.store.back.util.ResourceConstants;

@RestController
@RequestMapping(ResourceConstants.PRODUCT_RESOURCE)
public class ProductController {
	
	@Autowired
	private ProductSetvice productSetvice;
	
	@GetMapping
	public Page<Product> listProductByCategory(@RequestParam("id")int id, @RequestParam("page") Integer page){
		return productSetvice.finByCategory(id, PageRequest.of(page, ResourceConstants.PAGE));
	} 
	
	@GetMapping("/detail")
	public Product getProduct(@RequestParam("id") int id) {
		return productSetvice.findProduct(id);
	}
	
	@PostMapping
	public Product addProduct(@RequestBody Product product) {
		return productSetvice.add(product);
	}
	
	@PutMapping
	public Product editCategory(@RequestBody Product product) {
		return productSetvice.edit(product);
	}
	
	@DeleteMapping
	public Product deleteCatego(@RequestParam("id") int id ) {
		return productSetvice.delete(id);
	}

}
