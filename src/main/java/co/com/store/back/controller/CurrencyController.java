package co.com.store.back.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.store.back.util.ResourceConstants;

@RestController
@RequestMapping(ResourceConstants.CURRENCY_RESOURCE)
public class CurrencyController {
	
	@GetMapping
	public String getCurrency(@RequestParam("value")Double value) {
		try {
			URL url = new URL("https://api.cambio.today/v1/quotes/USD/COP/json?quantity="+value+"&key=2824|xn4ERpjeGjaXZi2o2f^nTw4^2XAFLDW4");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
			if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            String result ="";
            while ((output = br.readLine()) != null) {
                System.out.println(output);
                result += output;
            }
            conn.disconnect();
			return result;
		} catch (Exception e) {
			return e.toString();
		} 
	}

}
