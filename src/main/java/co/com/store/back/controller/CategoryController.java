package co.com.store.back.controller;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.store.back.entity.Category;
import co.com.store.back.service.CategoryService;
import co.com.store.back.util.ResourceConstants;

@RestController
@RequestMapping(ResourceConstants.CATEGORY_RESOURCE)
public class CategoryController {
	
	@Autowired
	private CategoryService categoryService;
	
	@GetMapping
	public Page<Category> listCategoryFather(@RequestParam("page") Integer page) {
		return categoryService.findFather(PageRequest.of(page, ResourceConstants.PAGE));
	}
	
	@GetMapping("/")
	public Page<Category> listByFather(@RequestParam("id") int id, @RequestParam("page") Integer page) {
		return categoryService.findByFather(id, PageRequest.of(page, ResourceConstants.PAGE));
	}
	
	@GetMapping("/son")
	public List<Category> listSon() {
		return categoryService.findFinalSon();
	}
	
	@GetMapping("/back")
	public int getBack(@RequestParam("id") int id ) {
		return categoryService.getBack(id);
	}
	
	@GetMapping("/sons")
	public int getSons(@RequestParam("id") int id ) {
		return categoryService.countSons(id);
	}
	
	@GetMapping("/all")
	public List<Category> getAll() {
		return categoryService.findAll();
	}
	
	@PostMapping
	public Category addCategory(@RequestBody Category category) {
		return categoryService.add(category);
	}
	
	@PutMapping
	public Category editCategory(@RequestBody Category category) {
		return categoryService.edit(category);
	}
	
	@DeleteMapping
	public Category deleteCategory(@RequestParam("id") int id ) {
		return categoryService.delete(id);
	}

}
