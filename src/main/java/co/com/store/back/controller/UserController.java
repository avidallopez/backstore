package co.com.store.back.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.store.back.entity.User;
import co.com.store.back.service.UserService;
import co.com.store.back.util.ResourceConstants;

@RestController
@RequestMapping(ResourceConstants.USER_RESOURCE)
public class UserController {

	@Autowired
	private UserService userService;
	
	@GetMapping
	public List<User> listUser() {
		return userService.listAll();
	}
	
	@GetMapping("/")
	public User getUserById(@RequestParam("id") String id) {
		return userService.findUser(id);
	}
	
	@PostMapping
	public User add(@RequestBody User user) {
		return userService.add(user);
	}
	
	@PutMapping
	public User edit(@RequestBody User user ) {
		return userService.edit(user);
	}
	
	@DeleteMapping("/")
	public User deleteUser(@RequestParam("id") String id) {
		return userService.delete(id);
	}
	
}
